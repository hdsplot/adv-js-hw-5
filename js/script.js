const API = 'https://ajax.test-danit.com/api/json/';

const root = document.querySelector('#root')
const modal = document.createElement('div')
modal.classList.add('modal')
document.body.append(modal)
const sendRequest = async (entity, method = 'GET', config) => {
    return await fetch(`${API}${entity}`, {
        method,
        ...config
    })
        .then((response) => {
            if (response.ok) {
                if (method === 'GET' || method === 'POST' || method === 'PUT') {
                    return response.json()
                }
                return response
            }
        })
};

const getUsers = () => sendRequest('users');
const getPosts = (id) => sendRequest(`users/${id}/posts`);
const delPost = (id) => sendRequest(`posts/${id}`, 'DELETE');
const editPost = (id, requestBody) => sendRequest(`posts/${id}`, 'PUT', {
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify(requestBody)

})

class Card {
    constructor(name, id, email, title, body, userId) {
        this.name = name;
        this.email = email;
        this.id = id;
        this.title = title;
        this.body = body;
        this.userId = userId
    }

    renderCard(cardWrapper) {
        const post = document.createElement('div')
        post.classList.add('post')
        post.id = `post-${this.id}`;

        const postAction = document.createElement('div')
        postAction.classList.add('post__action')
        postAction.innerHTML = '<button class="btn-del">del</button><button class="btn-edit">edit</button>'

        post.append(postAction)
        post.insertAdjacentHTML('beforeend', `
            <p class="post__author">${this.name}</p>
            <a class="post__email" href="mailto:${this.email}">${this.email}</a>
            <h4 class="post__title">${this.title}</h4>
            <p class="post__body">${this.body}</p>
        `)
        cardWrapper.append(post)
    }
}

class EditModal {
    constructor(title, body) {
        this.title = title;
        this.body = body;
    }

    renderModal(wrapper) {
        const modal = document.createElement('div');
        modal.classList.add('modal-wrapper')
        modal.insertAdjacentHTML('beforeend', `
            <div class="modal">    
                <form class="modal__form">
                    <label class="modal__title">Title
                    <input type="text" class="modal__input-title" id="title" value="${this.title}">
                    </label>
            
                    <label class="modal__body">Text
                    <textarea type="text" class="modal__input-body" id="body" rows="13">${this.body}</textarea>
                    </label>
            
                    <button id="btn-save">SAVE</button>
                </form>
            </div>

        `)
        wrapper.append(modal)
    }
}

getUsers().then((users) => {
    users.forEach(({ name, id, email }) => {
        getPosts(id)
            .then(data => {
                data.forEach(({ title, body, id, userId }) => {
                    const card = new Card(name, id, email, title, body, userId);
                    return card.renderCard(root)
                })
            })
    })
})

document.addEventListener('click', (e) => {
    if (e.target.className === 'btn-del') {
        const postId = e.target.closest('.post').getAttribute('id').slice(5);
        // console.log(postId);
        delPost(postId)
            .then(response => {
                if (response.ok) {
                    document.querySelector(`#post-${postId}`).remove()
                }
            })
    }

    if (e.target.className === 'btn-edit') {
        const post = e.target.closest('.post')
        const postId = post.getAttribute('id').slice(5);
        // console.log(postId);
        const editTitle = post.querySelector('.post__title').innerText;
        const editBody = post.querySelector('.post__body').innerText;
        const editModal = new EditModal(editTitle, editBody);
        editModal.renderModal(modal);
        root.classList.add('opacity')

        const btnSave = document.querySelector('#btn-save')
        btnSave.addEventListener('click', (e) => {
            e.preventDefault();
            const editForm = document.querySelector('form')
            const title = editForm.querySelector('input').value;
            const body = editForm.querySelector('textarea').value;
            // console.log(title);
            // console.log(body);
            const requestBody = {
                title,
                body
            };

            editPost(postId, requestBody)
                .then(response => {
                    if (response) {
                        post.querySelector('.post__title').innerText = title;
                        post.querySelector('.post__body').innerText = body;
                        document.querySelector('.modal-wrapper').remove()
                        root.classList.remove('opacity')
                    } 
                })
        })
    }
})
